class BooksController < ApplicationController
  def index
    @books = Book.order(created_at: :desc).all
  end

  def show
    @book = Book.find(params[:id])
    @reviews = @book.reviews
  end
end
