module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: ["plugin:prettier/recommended"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018
  },
  plugins: ["jest", "erb", "react"],
  rules: {
    "comma-dangle": [
      "warn",
      {
        arrays: "always",
        objects: "always",
        imports: "never",
        exports: "never",
        functions: "never"
      }
    ]
  }
};
